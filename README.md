# Automation Framework

This is a simple project for learning and experimentation purpose. This framework is made purely using 

- Python (3.7.3)
- Robot Framework
- PyTest

## Setting up Python Virtual Environment

To work with the python, we will be setting up a virtual environment using `virtualenvwrapper` on windows.

- Use the below command to download the `virtualenvwrapper` on windows:
  
  ```markdown
  pip install virtualenvwrapper-win
  ```

- Once `virtualenvwrapper` is installed, use the below command to create a new Virtual Environment:

  ```markdown
  mkvirtualenv HelloWorld
  ```

- Create the development directory for your project and bind the virtualenv to the project directory using the following command:

  ```markdown
  setprojectdir .
  ```

- To deactivate the environment, use the below command:
  
  ```markdown
  deactivate
  ```

- To start working on the Virtuel Environment, use the below command:
  
  ```markdown
  activate virtualenv-name
  ```
